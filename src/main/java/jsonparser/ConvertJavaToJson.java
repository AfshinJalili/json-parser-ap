package jsonparser;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

public class ConvertJavaToJson {
    private ArrayList<String> stringKeys = new ArrayList<>(JSONParserData.jsonStr.keySet());
    private ArrayList<String> numberKeys = new ArrayList<>(JSONParserData.jsonNumber.keySet());
    private ArrayList<String> booleanKeys = new ArrayList<>(JSONParserData.jsonBoolean.keySet());

    private void writeBoolean(HashMap<String, Boolean> booleanData, ArrayList<String> keys, PrintStream printStream) {
        for (String key : keys) {
            printStream.println("\t" + '\"' + key + '\"' + " : " + booleanData.get(key) + " ,");
        }
    }

    private void writeString(HashMap<String, String> stringData, ArrayList<String> keys, PrintStream printStream) {
        for (String key : keys) {
            printStream.println("\t" + '\"' + key + '\"' + " : " + '\"' + stringData.get(key) + '\"' + " ,");
        }
    }

    private void writeNumber(HashMap<String, String> numberData, ArrayList<String> keys, PrintStream printStream) {
        for (String key : keys) {
            printStream.println("\t" + '\"' + key + '\"' + " : " + numberData.get(key) + " ,");
        }
    }

    public void write(PrintStream printStream) {
        printStream.println("{");
        writeString(JSONParserData.jsonStr, stringKeys, printStream);
        writeNumber(JSONParserData.jsonNumber, numberKeys, printStream);
        writeBoolean(JSONParserData.jsonBoolean, booleanKeys, printStream);
        printStream.println("}");
    }



}
