package jsonparser;

import inputoutput.IO;

import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class ConvertJsonToJava {
    public static String jsonString = "";

    public static void convert(String jsonStr) {
        jsonString = jsonStr;
        String key;
        for (int charIndex = 0; charIndex < jsonString.length(); charIndex++) {
            if(jsonString.charAt(charIndex) == '\"') {
                key = jsonString.substring(charIndex+1, jsonString.indexOf('\"', charIndex+1));
                charIndex = jsonString.indexOf(':', charIndex) +1;
                if (jsonString.charAt(charIndex) == '\"') {
                    setJsonStr(key, jsonString.substring(charIndex+1, jsonString.indexOf('\"', charIndex+1)));
                    charIndex = jsonString.indexOf('\"', charIndex+1) +1;
                } else if ((jsonString.charAt(charIndex) >= '0') && (jsonString.charAt(charIndex) <= '9')) {
                    setJsonNumber(key, charIndex);
                    // todo update charIndex
                } else if (jsonString.charAt(charIndex) == 't' || jsonString.charAt(charIndex) == 'f') {
                    setJsonBoolean(key, jsonString.charAt(charIndex));
                    // todo update charIndex
                }
                // todo handle array and object
            }

        }

    }


    public static void setJsonStr(String key, String value) {
        JSONParserData.jsonStr.put(key, value);
    }

    public static void setJsonBoolean(String key, char value) {
        if (value == 't') { JSONParserData.jsonBoolean.put(key, true); }
        else if (value == 'f') { JSONParserData.jsonBoolean.put(key, false); }
    }


    public static void setJsonNumber(String key, int valueIndex) {
       StringBuilder value = new StringBuilder();
        do {
            value.append(jsonString.charAt(valueIndex));
            valueIndex++;
        } while (jsonString.charAt(valueIndex) != ',');

        JSONParserData.jsonNumber.put(key, value.toString());
    }

     public static void read() {
         Scanner scanner = new Scanner(IO.getInputStream());
         convert(scanner.nextLine().replaceAll("\n","").replaceAll(" ", ""));
     }

}
