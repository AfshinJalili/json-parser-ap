package inputoutput;

import java.io.InputStream;
import java.io.PrintStream;

public class IO {
    private static PrintStream printStream;
    private static InputStream inputStream;

    public static PrintStream getPrintStream() {
        return printStream;
    }

    public static void setPrintStream(PrintStream printStream) {
        IO.printStream = printStream;
    }

    public static InputStream getInputStream() {
        return inputStream;
    }

    public static void setInputStream(InputStream inputStream) {
        IO.inputStream = inputStream;
    }

    public static void setIO(PrintStream printStream, InputStream inputStream) {
        setInputStream(inputStream);
        setPrintStream(printStream);
    }
}
