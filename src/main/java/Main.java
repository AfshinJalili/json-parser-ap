import inputoutput.IO;
import jsonparser.ConvertJavaToJson;
import jsonparser.ConvertJsonToJava;
import jsonparser.JSONParserData;
import problem.SolveProblem;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        IO.setIO(System.out, System.in);
        ConvertJsonToJava.read();
        ConvertJavaToJson convertJavaToJson =new ConvertJavaToJson();
        convertJavaToJson.write(IO.getPrintStream());
    }
}
