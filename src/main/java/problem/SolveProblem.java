package problem;

import jsonparser.ConvertJsonToJava;
import jsonparser.JSONParserData;

public class SolveProblem {
    public static String action = JSONParserData.jsonStr.get("action");

    // todo recognize problem using a switch
    public static void solve() {
        switch (action) {
            case "fibonacci":
                Fibonacci.fibonacci();
                break;
            case "splitText":
                // todo call passwordGenerator method
                break;
            case "passwordGenerator":
                PasswordGenerator.generatePassword();
                break;
            case "sortNumber":
                // todo call sortNumber method
                break;
            case "studentInfo":
                // todo call studentInfo method
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + action);
        }
    }
}
