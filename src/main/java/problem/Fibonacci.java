package problem;

import inputoutput.IO;
import jsonparser.ConvertJsonToJava;
import jsonparser.JSONParserData;

import java.io.PrintStream;

public class Fibonacci {
     private static int index;

    public static int getIndex() {
        return index;
    }

    public static void fibonacci() {
         setData();
         write(IO.getPrintStream());
     }

    private static void setData() {
        Fibonacci.index = Integer.parseInt(JSONParserData.jsonNumber.get("index"));
    }

    private static long fibo(int n) {
        if ((n == 0) || (n == 1))
            return n;
        else
            return fibo(n - 1) + fibo(n - 2);
    }
    private static void write(PrintStream printStream) {
         printStream.println(fibo(getIndex()));
    }

}
