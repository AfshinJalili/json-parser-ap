package problem;

import inputoutput.IO;
import jsonparser.ConvertJsonToJava;
import jsonparser.JSONParserData;

import java.io.PrintStream;
import java.util.ConcurrentModificationException;
import java.util.Random;

public class PasswordGenerator {
    private static int length;
    private static boolean useAlphabet;
    private static boolean useNumber;

    public static void generatePassword() {
        setData();
        write(IO.getPrintStream());

    }

    private static void setData() {
        PasswordGenerator.length = Integer.parseInt(JSONParserData.jsonNumber.get("length"));
        PasswordGenerator.useAlphabet = JSONParserData.jsonBoolean.get("useAlphabet");
        PasswordGenerator.useNumber = JSONParserData.jsonBoolean.get("useNumber");
    }

    private static String generator() {
        StringBuilder password = new StringBuilder();
        Random random = new Random();
        if (useNumber) {
            if (useAlphabet) {
                password = new StringBuilder(generateRandomAlphabet(length - 1) + random.nextInt(10));
            } else {
                for (int i = 0; i < length; i++) {
                    password.append(random.nextInt(10));
                }
            }
        } else {
            password = new StringBuilder(generateRandomAlphabet(length));
        }
        return password.toString();
    }

    private static String generateRandomAlphabet(int passwordLength) {
        Random randomNum = new Random();
        StringBuilder password = new StringBuilder();
        for (int charIndex = 0; charIndex < passwordLength; charIndex++) {
            password.append((char) (randomNum.nextInt(25) + 97));
        }
        return password.toString();
    }

    private static void write(PrintStream printStream) {
        printStream.println(generator());
    }
}
